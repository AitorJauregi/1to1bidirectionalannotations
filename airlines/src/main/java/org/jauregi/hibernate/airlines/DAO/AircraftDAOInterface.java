package org.jauregi.hibernate.airlines.DAO;

import java.util.List;

import org.jauregi.hibernate.airlines.models.Aircraft;

public interface AircraftDAOInterface {
	public List<Aircraft> selectAll ();
	public void insert (Aircraft aircraft);
}
