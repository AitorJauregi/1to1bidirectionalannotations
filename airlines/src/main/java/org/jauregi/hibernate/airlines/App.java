package org.jauregi.hibernate.airlines;

import java.util.List;

import org.jauregi.hibernate.airlines.DAO.AircraftDAO;
import org.jauregi.hibernate.airlines.DAO.PilotDAO;
import org.jauregi.hibernate.airlines.models.Aircraft;
import org.jauregi.hibernate.airlines.models.Pilot;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Aircraft aircraft1 = new Aircraft("Boing 747", 4000f);	
		Aircraft aircraft2 = new Aircraft("Hercules",3500f);
		
		Pilot pilot1 = new Pilot("Juan",11111);
		Pilot pilot2 = new Pilot("Juan",22222);
		
		aircraft1.setPilot(pilot1);
		aircraft2.setPilot(pilot2);
		
		AircraftDAO aircraftDAO = new AircraftDAO();
		
		aircraftDAO.insert(aircraft1);
		aircraftDAO.insert(aircraft2);
		
		List<Aircraft> aircrafts = aircraftDAO.selectAll();
		
		 for ( Aircraft elAircraft : aircrafts ) {
			 System.out.println("Id: " + elAircraft.getId());
			 System.out.println("Model: " + elAircraft.getModel());
			 System.out.println("Autonomy: " + elAircraft.getAutonomy());
			 System.out.println("Pilot Name: " + elAircraft.getPilot().getName());
			 System.out.println("Pilot license: " + elAircraft.getPilot().getLicense());
		 }
    }
}
