package org.jauregi.hibernate.airlines.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jauregi.hibernate.airlines.HibernateSession;
import org.jauregi.hibernate.airlines.models.Pilot;

public class PilotDAO implements PilotDAOInterface{
	
	public void insert(Pilot pilot) {
		 SessionFactory sessionFactory = HibernateSession.getSessionFactory();
		    Session session = sessionFactory.openSession();
		    session.beginTransaction();
		
		    session.persist(pilot);
		         
		    session.getTransaction().commit();
		    session.close();
		
	}
}
