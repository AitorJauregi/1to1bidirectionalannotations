/**
 * 
 */
package org.jauregi.hibernate.airlines.DAO;

import org.jauregi.hibernate.airlines.models.Pilot;

/**
 * @author Aitor
 *
 */
public interface PilotDAOInterface {
	public void insert (Pilot pilot);
}
